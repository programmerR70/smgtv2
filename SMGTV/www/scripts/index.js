﻿// Основные сведения о пустом шаблоне см. в следующей документации:
// http://go.microsoft.com/fwlink/?LinkID=397704
// Для отладки кода при загрузке страницы в Ripple, а также на устройства или в эмуляторы Android запустите приложение, задайте точки останова, 
// , а затем запустите "window.location.reload()" в консоли JavaScript.
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Обработка событий приостановки и возобновления Cordova
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener('resume', onResume.bind(this), false);
        $('.b--container').addClass('run');
        setInterval(function () {
            $('.b--container').removeClass('run');
            var newimages = $('.b--images').clone(true),
                newscrollbar = $('.b--scrollbar').clone(true),
                newbg = $('.b--container__bg').clone(true),
                newwhite = $('.b--white').clone(true);
            $('.b--images').before(newimages).remove();
            $('.b--scrollbar').before(newscrollbar).remove();
            $('.b--container__bg').before(newbg).remove();
            $('.b--white').before(newwhite).remove();
            $('.b--container').addClass('run');
        },32000);
        var max = 1800,
            doc_max = 1920,
            scale = (($(document).width() - 120) / max).toFixed(2),
            scale_doc = (($(document).width()) / doc_max).toFixed(2),
            offset = ((1 - scale) * 100).toFixed(0);
        if(scale < 1){
            $('.b--infoblock').css('transform', 'scale(' + scale + ')');
            $('.b--images').css('transform', 'scale(' + scale_doc + ')');
        }

        $(window).resize(function () {
            $('.b--infoblock').css('transform', 'none');
            $('.b--images').css('transform', 'none');
            scale = (($(document).width() - 120) / max).toFixed(2);
            scale_doc = (($(document).width()) / doc_max).toFixed(2);
            $('.b--infoblock').css('transform', 'scale(' + scale + ')');
            $('.b--images').css('transform', 'scale(' + scale_doc + ')');
        });
    };

    function onPause() {
        // TODO: Это приложение приостановлено. Сохраните здесь состояние приложения.
    };

    function onResume() {
        // TODO: Это приложение активировано повторно. Восстановите здесь состояние приложения.
    };
} )();